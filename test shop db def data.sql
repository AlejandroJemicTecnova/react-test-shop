USE [TEST_SHOP]
GO
/****** Object:  Table [dbo].[sections]    Script Date: 09-01-2020 18:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sections](
	[title] [nvarchar](50) NULL,
	[imageUrl] [nvarchar](1000) NULL,
	[id] [int] NULL,
	[linkUrl] [nvarchar](50) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[sections] ([title], [imageUrl], [id], [linkUrl]) VALUES (N'hats', N'https://i.ibb.co/cvpntL1/hats.png', 1, N'shop/hats')
INSERT [dbo].[sections] ([title], [imageUrl], [id], [linkUrl]) VALUES (N'jackets', N'https://i.ibb.co/px2tCc3/jackets.png', 2, N'shop/jackets')
INSERT [dbo].[sections] ([title], [imageUrl], [id], [linkUrl]) VALUES (N'sneakers', N'https://i.ibb.co/0jqHpnp/sneakers.png', 3, N'shop/sneakers')
INSERT [dbo].[sections] ([title], [imageUrl], [id], [linkUrl]) VALUES (N'womens', N'https://i.ibb.co/GCCdy8t/womens.png', 4, N'shop/womens')
INSERT [dbo].[sections] ([title], [imageUrl], [id], [linkUrl]) VALUES (N'mens', N'https://i.ibb.co/R70vBrQ/men.png', 5, N'shop/mens')
