import SHOP_DATA from "../shop.data";

export default {
  getShopData(req, res) {
    console.log(`GET /collections`);
    res.setHeader("Content-Type", "application/json");
    res.send(JSON.stringify(SHOP_DATA, null, 3));
  }
};
