//hardcode data return
import SECTION_DATA from "../sections.data";
import selectSections from "../Sql/sql.provider";

export default {
  getSectionsData(req, res) {
    console.log(`GET /sections`);
    res.setHeader("Content-Type", "application/json");
    res.send(JSON.stringify(SECTION_DATA, null, 3));
  },
  getSectionsSQL(req, res) {
    console.log(`GET /sections`);
    res.setHeader("Content-Type", "application/json");
    selectSections(resData => {
      res.send(JSON.stringify(resData, null, 3));
    });
  }
};
