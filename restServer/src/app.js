import express from "express";
import { router } from "./Config/router";

const app = express();
const PORT = 3500;

// handle api routes
app.use("/api", router);

// server alive
app.get("/", (req, res) => {
  console.log("GET /");
  res.json({
    msg: "shop rest sever GET ok"
  });
});

// block all not handled routes
app.all("/*", function(req, res) {
  console.log("bloqued ", req.originalUrl);
  res.status(403).send({
    msg: `Access Forbidden to ${req.originalUrl}`
  });
});

app.listen(PORT, () => console.log(`rest server listening on ${PORT}`));
