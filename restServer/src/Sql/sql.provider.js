import sql from "mssql";
import config from "../Sql/conecction.config";

const selectSections = fn => {
  sql.connect(config, function(err) {
    if (err) {
      console.log(err);
      return fn({ sections: [], err: err });
    }

    var request = new sql.Request();
    request.query("select * from sections", (err, sqlData) => {
      if (err) {
        console.log(err);
        return fn({ sections: [], err: err });
      }

      console.table(sqlData.recordset);
      return fn({ sections: sqlData.recordset, err: null });
    });
  });
};

export default selectSections;
