import express from "express";
import CollectionsController from "../Controllers/collections.controller";
import sectionsController from "../Controllers/sections.controller";

export const router = express.Router();

router.get(`/collections`, CollectionsController.getShopData);
router.get(`/sections/hc`, sectionsController.getSectionsData); // with hardcode data
router.get(`/sections`, sectionsController.getSectionsSQL); // with SQL data
