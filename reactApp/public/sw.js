// importScripts("/js/idbPromiseProvider.js");
// importScripts("/js/idbUtils.js");

var CACHE_STATIC_NAME = "CRWN-static-v6";
var CACHE_DYNAMIC_NAME = "CRWN-dynamic-v6";
var STATIC_FILES = [
  "/",
  "/index.html",
  "/offline.html",
  "/js/idbPromiseProvider.js",
  "/js/promiseProvider.js",
  "/js/fetchProvider.js",
  "/js/idbUtils.js",
  "/manifest.json",
  "favicon.ico",
  "/Icons/android-icon-192x192.png",
  "/Icons/android-icon-96x96.png",
  "/Icons/apple-icon-114x114.png",
  "/Icons/apple-icon-120x120.png",
  "/Icons/apple-icon-180x180.png",
  "/Icons/apple-icon-192x192.png",
  "/Icons/apple-icon-512x512.png",
  "/Icons/apple-icon-57x57.png",
  "/Icons/apple-icon-60x60.png",
  "/Icons/apple-icon-72x72.png",
  "/Icons/apple-icon-76x76.png",
  "/Icons/apple-icon-precomposed.png",
  "/Icons/apple-icon.png",
  "/Icons/favicon-16x16.png",
  "/Icons/favicon-32x32.png",
  "/Icons/favicon-96x96.png",
  "/Icons/ms-icon-310x310.png",
  "/Icons/ms-icon-70x70.png",
  "https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300"
];
var CACHEBLE_REQS = ["/api", "/static", ".png", ".jpg", ".PNG", ".JPG", ".ico"];

self.addEventListener("install", function(event) {
  console.log("[Service Worker] Installing Service Worker ...", event);
  event.waitUntil(
    caches.open(CACHE_STATIC_NAME).then(function(cache) {
      console.log("[Service Worker] Precaching App Shell");
      cache.addAll(STATIC_FILES);
    })
  );
});

function isInArray(string, array) {
  var cachePath;
  if (string.indexOf(self.origin) === 0) {
    // request targets domain where we serve the page from (i.e. NOT a CDN)
    console.log("matched ", string);
    cachePath = string.substring(self.origin.length); // take the part of the URL AFTER the domain (e.g. after localhost:8080)
  } else {
    cachePath = string; // store the full request (for CDNs)
  }
  return array.indexOf(cachePath) > -1;
}

// network with cache fallback and dinamic cache responses
self.addEventListener("fetch", function(event) {
  if (navigator.onLine) {
    // si online intento fetch
    event.respondWith(
      fetch(event.request)
        .then(function(res) {
          return caches.open(CACHE_DYNAMIC_NAME).then(function(cache) {
            //si fetch ok guarda al cache y retorna // solo se cachean y responden los request http o https
            if (event.request.url.indexOf("http") === 0) {
              cache.put(event.request.url, res.clone());
              return res;
            } else return;
          });
        })
        .catch(function(err) {
          return caches.match(event.request); // si falla busca en los cache, ojo como es un catch genera falsos errores
        })
    );
  } else {
    event.respondWith(caches.match(event.request)); // si ofline solo cahce
  }
});

// limpia el cache cuado se actualiza la version oy s reinstalas el sw
self.addEventListener("activate", function(event) {
  console.log("[Service Worker] Activating Service Worker ....", event);
  event.waitUntil(
    caches.keys().then(function(keyList) {
      return Promise.all(
        keyList.map(function(key) {
          if (key !== CACHE_STATIC_NAME && key !== CACHE_DYNAMIC_NAME) {
            console.log("[Service Worker] Removing old cache.", key);
            return caches.delete(key);
          }
        })
      );
    })
  );
  return self.clients.claim();
});
