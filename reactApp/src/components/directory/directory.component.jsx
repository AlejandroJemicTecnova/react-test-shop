import React from "react";

import { DirectoryMenuContainer } from "./directory.styles";
import MenuItem from "../menu-item/menu-item.component";

class Directory extends React.Component {
  constructor() {
    super();
    this.state = {
      sections: []
    };
  }

  componentDidMount() {
    fetch(`/api/sections`)
      .then(response => response.json())
      .then(SectionsData => {
        this.setState({ sections: SectionsData.sections });
      })
      .catch(err => console.log(err));
  }

  render() {
    const { sections } = this.state;
    return (
      <DirectoryMenuContainer>
        {sections.length > 0 ? (
          sections.map(({ id, ...otherSectionProps }) => (
            <MenuItem key={id} {...otherSectionProps} />
          ))
        ) : (
          <h3>Loading ...</h3>
        )}
      </DirectoryMenuContainer>
    );
  }
}

export default Directory;
