// import DirectoryActionTypes from "./directory.types";

// export const fetchSectionsStart = () => ({
//   type: DirectoryActionTypes.FETCH_SECTIONS_START
// });

// export const fetchSectionsSuccess = SectionsMap => ({
//   type: DirectoryActionTypes.FETCH_SECTIONS_SUCCESS,
//   payload: SectionsMap
// });

// export const fetchSectionsFailure = errorMessage => ({
//   type: DirectoryActionTypes.FETCH_SECTIONS_FAILURE,
//   payload: errorMessage
// });

// export const fetchSectionsStartAsync = () => {
//   return dispatch => {
//     dispatch(fetchSectionsStart());

//     //fetch("localhost:3500/sections", { mode: "no-cors" })    // se pasa a proxy en package.json, solo para localhost
//     fetch("/api/sections", { mode: "no-cors" })
//       .then(res => res.json())
//       .then(sectionsMap => {
//         dispatch(fetchSectionsSuccess(sectionsMap));
//       })
//       .catch(error => dispatch(fetchSectionsFailure(error.message)));
//   };
// };
