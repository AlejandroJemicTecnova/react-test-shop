import ShopActionTypes from "./shop.types";

// import {
//   firestore,
//   convertCollectionsSnapshotToMap
// } from "../../firebase/firebase.utils";

export const fetchCollectionsStart = () => ({
  type: ShopActionTypes.FETCH_COLLECTIONS_START
});

export const fetchCollectionsSuccess = collectionsMap => ({
  type: ShopActionTypes.FETCH_COLLECTIONS_SUCCESS,
  payload: collectionsMap
});

export const fetchCollectionsFailure = errorMessage => ({
  type: ShopActionTypes.FETCH_COLLECTIONS_FAILURE,
  payload: errorMessage
});

export const fetchCollectionsStartAsync = () => {
  return dispatch => {
    //const collectionRef = firestore.collection("collections");
    dispatch(fetchCollectionsStart());

    //fetch("localhost:3500/collections", { mode: "no-cors" })    // se pasa a proxy en package.json, solo para localhost
    fetch("/api/collections", { mode: "no-cors" })
      .then(res => res.json())
      .then(collectionsMap => {
        dispatch(fetchCollectionsSuccess(collectionsMap));
      })
      .catch(error => dispatch(fetchCollectionsFailure(error.message)));
  };
};
